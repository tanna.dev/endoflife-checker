package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"

	"gitlab.com/tanna.dev/endoflife-checker/awselasticache"
	"golang.org/x/sync/errgroup"

	"github.com/aws/aws-sdk-go-v2/aws/arn"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/elasticache"
	awselasticachetypes "github.com/aws/aws-sdk-go-v2/service/elasticache/types"
)

func performJSONReport(outDir string) {
	ctx := context.Background()

	err := os.MkdirAll(outDir, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	var eg errgroup.Group
	svc := elasticache.NewFromConfig(cfg)

	var marker *string

	for {
		out, err := svc.DescribeCacheClusters(ctx, &elasticache.DescribeCacheClustersInput{
			Marker: marker,
		})
		if err != nil {
			log.Fatalf("unable to list Elasticache Clusters, %v", err)
		}

		for _, cluster := range out.CacheClusters {
			cluster := cluster
			tags, err := svc.ListTagsForResource(ctx, &elasticache.ListTagsForResourceInput{
				ResourceName: cluster.ARN,
			})
			if err != nil {
				log.Fatal(err)
			}

			eg.Go(func() error {
				return writeClusterJSONReport(outDir, cluster, tags.TagList)
			})
		}

		marker = out.Marker

		if marker == nil {
			break
		}
	}

	err = eg.Wait()
	if err != nil {
		log.Fatal(err)
	}
}

func writeClusterJSONReport(outDir string, cluster awselasticachetypes.CacheCluster, tagList []awselasticachetypes.Tag) error {
	if cluster.ARN == nil {
		return fmt.Errorf("ElastiCache Cluster did not contain an ARN")
	}

	clusterArn, err := arn.Parse(*cluster.ARN)
	if err != nil {
		return err
	}

	if cluster.CacheClusterId == nil {
		return fmt.Errorf("instance with ARN %s did not contain a CacheClusterId", clusterArn.String())
	}

	filename := fmt.Sprintf("%s-%s-%s-cluster.json", clusterArn.AccountID, clusterArn.Region, *cluster.CacheClusterId)

	var r awselasticache.Report

	r.Database.AccountId = clusterArn.AccountID
	r.Database.Region = clusterArn.Region

	r.Database.ARN = clusterArn.String()

	r.Database.Name = *cluster.CacheClusterId

	if cluster.Engine == nil {
		return fmt.Errorf("Cluster with ARN %s did not have an Engine", clusterArn.String())
	}
	r.Database.Engine.Name = *cluster.Engine

	if cluster.EngineVersion == nil {
		return fmt.Errorf("Cluster with ARN %s did not have an EngineVersion", clusterArn.String())
	}
	r.Database.Engine.Version = *cluster.EngineVersion

	r.Database.Tags = make(map[string]string)

	for _, t := range tagList {
		if t.Key == nil || t.Value == nil {
			return fmt.Errorf("Cluster with ARN %s contains some tags with nil key/values: %v", clusterArn.String(), tagList)
		}

		r.Database.Tags[*t.Key] = *t.Value
	}

	data, err := json.Marshal(r)
	if err != nil {
		return err
	}

	err = os.WriteFile(path.Join(outDir, filename), data, 0644)
	if err != nil {
		return err
	}

	return nil
}
