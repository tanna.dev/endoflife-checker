package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"

	l "gitlab.com/tanna.dev/endoflife-checker/awslambda"
	"golang.org/x/sync/errgroup"

	"github.com/aws/aws-sdk-go-v2/aws/arn"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/lambda"
	awslambdatypes "github.com/aws/aws-sdk-go-v2/service/lambda/types"
)

func performJSONReport(outDir string) {
	ctx := context.Background()

	err := os.MkdirAll(outDir, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	var eg errgroup.Group
	svc := lambda.NewFromConfig(cfg)

	var marker *string

	for {
		out, err := svc.ListFunctions(ctx, &lambda.ListFunctionsInput{
			Marker: marker,
		})
		if err != nil {
			log.Fatalf("unable to list functions, %v", err)
		}

		for _, fc := range out.Functions {
			fc := fc

			eg.Go(func() error {
				gfo, err := svc.GetFunction(ctx, &lambda.GetFunctionInput{
					FunctionName: fc.FunctionName,
				})
				if err != nil {
					return err
				}

				return writeJSONReport(outDir, fc, gfo)
			})
		}

		marker = out.NextMarker

		if marker == nil {
			break
		}
	}

	err = eg.Wait()
	if err != nil {
		log.Fatal(err)
	}
}

func writeJSONReport(outDir string, fc awslambdatypes.FunctionConfiguration, gfo *lambda.GetFunctionOutput) error {
	funcArn, err := arn.Parse(*fc.FunctionArn)
	if err != nil {
		return err
	}

	filename := fmt.Sprintf("%s-%s-%s.json", funcArn.AccountID, funcArn.Region, *fc.FunctionName)

	var r l.Report
	r.Function.AccountId = funcArn.AccountID
	r.Function.Region = funcArn.Region
	r.Function.ARN = funcArn.String()
	r.Function.Name = *fc.FunctionName
	r.Function.Runtime = string(fc.Runtime)
	r.Function.LastModified = fc.LastModified
	r.Function.Tags = gfo.Tags

	data, err := json.Marshal(r)
	if err != nil {
		return err
	}

	err = os.WriteFile(path.Join(outDir, filename), data, 0644)
	if err != nil {
		return err
	}

	return nil
}
