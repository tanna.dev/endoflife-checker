package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/lambda"
	"github.com/aws/aws-sdk-go-v2/service/lambda/types"
	_ "gitlab.com/tanna.dev/endoflife-checker/awslambda/runtimes"
	"gitlab.com/tanna.dev/endoflife-checker/endoflifedate"
)

func main() {
	client, err := endoflifedate.NewClientWithResponses("https://endoflife.date")
	if err != nil {
		fmt.Printf("Could not initialise client: %v", err)
		os.Exit(1)
	}

	var tsvReport bool
	var jsonReport bool
	var runtimeName string
	var outDir string
	flag.BoolVar(&tsvReport, "report", false, "whether to output a Tab Separated Value (TSV) formatted report")
	flag.BoolVar(&jsonReport, "json", false, "whether to output a JSON-formatted report that can be used with https://dmd.tanna.dev")
	flag.StringVar(&runtimeName, "runtime", "", "Which runtime to lookup data for")
	flag.StringVar(&outDir, "out", "out", "Which directory to output JSON-formatted reports to")

	flag.Parse()

	if tsvReport {
		performTsvReport(client)
	} else if jsonReport {
		performJSONReport(outDir)
	} else if runtimeName != "" {
		performRuntimeLookup(client, runtimeName)
	} else {
		fmt.Fprintf(os.Stderr, "Invalid flags")
		flag.Usage()
		os.Exit(1)
	}
}

func performTsvReport(client *endoflifedate.ClientWithResponses) {
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	svc := lambda.NewFromConfig(cfg)

	var functions []types.FunctionConfiguration
	var rows []Row
	var marker *string

	for {
		out, err := svc.ListFunctions(context.Background(), &lambda.ListFunctionsInput{
			Marker: marker,
		})
		if err != nil {
			log.Fatalf("unable to list functions, %v", err)
		}

		functions = append(functions, out.Functions...)
		marker = out.NextMarker

		if marker == nil {
			break
		}
	}

	for _, f := range functions {
		fout, err := svc.GetFunction(context.Background(), &lambda.GetFunctionInput{
			FunctionName: f.FunctionName,
		})
		if err != nil {
			log.Fatalf("unable to list functions, %v", err)
		}

		product, cycle, found := ProductCycleFromRuntime(fout.Configuration.Runtime)
		var c *endoflifedate.Cycle
		if found {
			resp, err := client.GetApiProductCycleJsonWithResponse(context.Background(), product, cycle)
			if err != nil {
				log.Printf("unable to lookup endoflife data for %s %s: %v", product, cycle, err)
			}
			c = resp.JSON200
		}

		rows = append(rows, NewRow(cfg.Region, f, *fout, c))
	}

	printTSV(RowHeaders)
	for _, r := range rows {
		printTSV(r.ToCols())
	}
}

func performRuntimeLookup(client *endoflifedate.ClientWithResponses, runtime string) {
	product, cycle, found := ProductCycleFromRuntime(types.Runtime(runtime))
	if !found {
		fmt.Fprintf(os.Stderr, "Runtime %s does not have a mapping to endoflife.date data. Maybe raise an issue?\n", runtime)
		os.Exit(1)
	}

	resp, err := client.GetApiProductCycleJsonWithResponse(context.Background(), product, cycle)
	if err != nil {
		fmt.Fprintf(os.Stderr, "unable to lookup endoflife data for %s %s: %v\n", product, cycle, err)
		os.Exit(1)
	}

	c := resp.JSON200
	if c != nil {
		now := time.Now()

		if c.Support != nil {
			deprecated, err := c.Support.AsCycleSupport0()
			if err != nil {
				log.Printf("failed to extract Deprecation data from endoflife call for runtime %s: %v", runtime, err)
			} else {
				isDeprecated := now.After(deprecated.Time)
				days := betweenDates(now, &deprecated.Time)
				formatted := deprecated.Format(time.RFC3339)
				if isDeprecated {
					fmt.Printf("`%s`'s underlying language has been deprecated for %d days (since %s)\n", runtime, -days, formatted)
				} else {
					fmt.Printf("`%s`'s underlying language will be deprecated in %d days (on %s)\n", runtime, days, formatted)
				}
			}
		}

		eol, err := c.Eol.AsCycleEol0()
		if err != nil {
			log.Printf("failed to extract EOL data from endoflife call for runtime %s: %v", runtime, err)
		} else {
			eolDate, err := time.Parse("2006-01-02", eol)
			if err != nil {
				log.Printf("failed to extract End of Life data from endoflife call for runtime %s: %v", runtime, err)
			} else {
				isEndOfLife := now.After(eolDate)
				days := betweenDates(now, &eolDate)
				formatted := eolDate.Format(time.RFC3339)
				if isEndOfLife {
					fmt.Printf("`%s`'s underlying language has been end-of-life for %d days (since %s)\n", runtime, -days, formatted)
				} else {
					fmt.Printf("`%s`'s underlying language will be end-of-life in %d days (on %s)\n", runtime, days, formatted)
				}
			}
		}

	}
}
