package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/lambda"
	"github.com/aws/aws-sdk-go-v2/service/lambda/types"
	"gitlab.com/tanna.dev/endoflife-checker/awslambda/runtimes"
	"gitlab.com/tanna.dev/endoflife-checker/endoflifedate"
)

type Deprecation struct {
	IsDeprecated      bool
	DaysToDeprecation int
	DeprecationDate   string
}

type EndOfLife struct {
	IsEndOfLife     bool
	DaysToEndOfLife int
	EndOfLifeDate   string
}

type Row struct {
	Region       string
	FunctionName string
	FunctionArn  string
	Runtime      string
	LastModified string
	Tags         map[string]string

	RuntimeDeprecation Deprecation
	RuntimeEndOfLife   EndOfLife

	LanguageDeprecation Deprecation
	LanguageEndOfLife   EndOfLife
}

func (r Row) ToCols() []string {
	cols := []string{
		r.Region,
		r.FunctionName,
		r.FunctionArn,
		r.Runtime,
		r.LastModified,

		fmt.Sprintf("%v", r.RuntimeDeprecation.IsDeprecated),
		fmt.Sprintf("%d", r.RuntimeDeprecation.DaysToDeprecation),
		r.RuntimeDeprecation.DeprecationDate,

		fmt.Sprintf("%v", r.RuntimeEndOfLife.IsEndOfLife),
		fmt.Sprintf("%d", r.RuntimeEndOfLife.DaysToEndOfLife),
		r.RuntimeEndOfLife.EndOfLifeDate,

		fmt.Sprintf("%v", r.LanguageDeprecation.IsDeprecated),
		fmt.Sprintf("%d", r.LanguageDeprecation.DaysToDeprecation),
		r.LanguageDeprecation.DeprecationDate,

		fmt.Sprintf("%v", r.LanguageEndOfLife.IsEndOfLife),
		fmt.Sprintf("%d", r.LanguageEndOfLife.DaysToEndOfLife),
		r.LanguageEndOfLife.EndOfLifeDate,
	}

	if r.Tags != nil {
		tagsBytes, err := json.Marshal(r.Tags)
		if err == nil {
			cols = append(cols, string(tagsBytes))
		}
	}

	return cols
}

var RowHeaders []string = []string{
	"Region",
	"Function Name",
	"Function ARN",
	"Runtime",
	"Last Modified",

	"Is Deprecated Runtime? (Phase 1)",
	"Days Until Deprecation (Phase 1)",
	"Deprecation Date (Phase 1)",

	"Is End Of Life Runtime? (Phase 2)",
	"Days Until End of Life (Phase 2)",
	"End of Life Date (Phase 2)",

	"Is Deprecated Language?",
	"Days Until Deprecation",
	"Deprecation Date",

	"Is End Of Life Language?",
	"Days Until End of Life",
	"End of Life Date",

	"Tags",
}

func NewRow(region string, fc types.FunctionConfiguration, gfo lambda.GetFunctionOutput, cycle *endoflifedate.Cycle) Row {
	row := Row{
		Region:       region,
		FunctionName: safeString(fc.FunctionName),
		FunctionArn:  safeString(fc.FunctionArn),
		Runtime:      string(fc.Runtime),
		LastModified: safeString(fc.LastModified),
		// defaults, so reporting doesn't panic when it sees it as the default of 0
		RuntimeDeprecation: Deprecation{
			DaysToDeprecation: 100_000,
		},
		RuntimeEndOfLife: EndOfLife{
			DaysToEndOfLife: 100_000,
		},
		LanguageDeprecation: Deprecation{
			DaysToDeprecation: 100_000,
		},
		LanguageEndOfLife: EndOfLife{
			DaysToEndOfLife: 100_000,
		},

		Tags: gfo.Tags,
	}

	data, found := runtimes.IsDeprecated(fc.Runtime)
	if found {
		now := time.Now()

		if data.Deprecation != nil {
			row.RuntimeDeprecation.IsDeprecated = now.After(*data.Deprecation)
			row.RuntimeDeprecation.DeprecationDate = data.Deprecation.Format(time.RFC3339)
			row.RuntimeDeprecation.DaysToDeprecation = betweenDates(now, data.Deprecation)
		}

		if data.EndOfLife != nil {
			row.RuntimeEndOfLife.IsEndOfLife = now.After(*data.EndOfLife)
			row.RuntimeEndOfLife.EndOfLifeDate = data.EndOfLife.Format(time.RFC3339)
			row.RuntimeEndOfLife.DaysToEndOfLife = betweenDates(now, data.EndOfLife)
		}
	}

	if cycle != nil {
		now := time.Now()

		if cycle.Support != nil {
			deprecated, err := cycle.Support.AsCycleSupport0()
			if err != nil {
				log.Printf("failed to extract Deprecation data from endoflife call for function %s: %v", row.FunctionName, err)
			} else {
				row.LanguageDeprecation.IsDeprecated = now.After(deprecated.Time)
				row.LanguageDeprecation.DeprecationDate = deprecated.Format(time.RFC3339)
				row.LanguageDeprecation.DaysToDeprecation = betweenDates(now, &deprecated.Time)
			}
		}

		eol, err := cycle.Eol.AsCycleEol0()
		if err != nil {
			log.Printf("failed to extract EOL data from endoflife call for function %s: %v", row.FunctionName, err)
		} else {
			eolDate, err := time.Parse("2006-01-02", eol)
			if err != nil {
				log.Printf("failed to parse EOL date %s as date, from endoflife call for function %s: %v", eol, row.FunctionName, err)
			} else {
				row.LanguageEndOfLife.IsEndOfLife = now.After(eolDate)
				row.LanguageEndOfLife.EndOfLifeDate = eolDate.Format(time.RFC3339)
				row.LanguageEndOfLife.DaysToEndOfLife = betweenDates(now, &eolDate)
			}
		}
	}

	return row
}

func printTSV(cols []string) {
	fmt.Println(strings.Join(cols, "\t"))
}
