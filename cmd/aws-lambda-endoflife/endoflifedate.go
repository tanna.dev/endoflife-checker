package main

import "github.com/aws/aws-sdk-go-v2/service/lambda/types"

func ProductCycleFromRuntime(runtime types.Runtime) (product string, cycle string, found bool) {
	switch runtime {
	case types.RuntimeNodejs:
		// not supported
		return
	case types.RuntimeNodejs43:
		return "nodejs", "4", true
	case types.RuntimeNodejs610:
		return "nodejs", "6", true
	case types.RuntimeNodejs810:
		return "nodejs", "8", true
	case types.RuntimeNodejs10x:
		return "nodejs", "10", true
	case types.RuntimeNodejs12x:
		return "nodejs", "12", true
	case types.RuntimeNodejs14x:
		return "nodejs", "14", true
	case types.RuntimeNodejs16x:
		return "nodejs", "16", true
	case types.RuntimeJava8:
		return "java", "8", true
	case types.RuntimeJava8al2:
		return "java", "8", true
	case types.RuntimeJava11:
		return "java", "11", true
	case types.RuntimePython27:
		return "python", "2.7", true
	case types.RuntimePython36:
		return "python", "3.6", true
	case types.RuntimePython37:
		return "python", "3.7", true
	case types.RuntimePython38:
		return "python", "3.8", true
	case types.RuntimePython39:
		return "python", "3.9", true
	case types.RuntimeDotnetcore10:
		return "python", "3.9", true
	case types.RuntimeDotnetcore20:
		// not supported
		return
	case types.RuntimeDotnetcore21:
		// not supported
		return
	case types.RuntimeDotnetcore31:
		// not supported
		return
	case types.RuntimeDotnet6:
		// not supported
		return
	case types.RuntimeNodejs43edge:
		return "nodejs", "4", true
	case types.RuntimeGo1x:
		// not supported
		return
	case types.RuntimeRuby25:
		return "ruby", "2.5", true
	case types.RuntimeRuby27:
		return "ruby", "2.7", true
	case types.RuntimeProvided:
		// not supported
		return
	case types.RuntimeProvidedal2:
		// not supported
		return
	case types.RuntimeNodejs18x:
		return "nodejs", "18", true
	}
	return
}
