package main

import "time"

func betweenDates(t1 time.Time, t2 *time.Time) int {
	hours := t2.Sub(t1).Hours()
	return int(hours) / 24
}
