package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/tanna.dev/endoflife-checker/awsrds/engines"
)

func main() {
	var jsonReport bool
	var engineName string
	var engineVersion string
	var outDir string
	flag.BoolVar(&jsonReport, "json", false, "whether to output a JSON-formatted report that can be used with https://dmd.tanna.dev")
	flag.StringVar(&engineName, "engine", "", "Engine name, i.e. `aurora-postgresql`")
	flag.StringVar(&engineVersion, "engineVersion", "", "Engine version i.e. `12.13`")
	flag.StringVar(&outDir, "out", "out", "Which directory to output JSON-formatted reports to")

	flag.Parse()

	if jsonReport {
		performJSONReport(outDir)
	} else if engineName != "" {
		performEngineLookup(engineName, engineVersion)
	} else {
		fmt.Fprintf(os.Stderr, "Invalid flags")
		flag.Usage()
		os.Exit(1)
	}
}

func performEngineLookup(name, version string) {
	deprecation, found := engines.IsDeprecated(name, version)
	if !found {
		fmt.Printf("Couldn't find any deprecation information for %s version %s - could mean that this needs a data update, or could mean it's not deprecated\n", name, version)
		return
	}

	now := time.Now()

	isDeprecated := now.After(deprecation.Deprecation)
	days := betweenDates(now, &deprecation.Deprecation)
	formatted := deprecation.Deprecation.Format(time.RFC3339)
	if isDeprecated {
		fmt.Printf("`%s` @ %s has been deprecated for %d days (since %s)\n", name, version, -days, formatted)
	} else {
		fmt.Printf("`%s` @ %s will be deprecated in %d days (on %s)\n", name, version, days, formatted)
	}
}
