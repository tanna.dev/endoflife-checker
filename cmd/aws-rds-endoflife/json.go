package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"

	"golang.org/x/sync/errgroup"

	"github.com/aws/aws-sdk-go-v2/aws/arn"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/rds"
	awsrdstypes "github.com/aws/aws-sdk-go-v2/service/rds/types"
	"gitlab.com/tanna.dev/endoflife-checker/awsrds"
)

func performJSONReport(outDir string) {
	ctx := context.Background()

	err := os.MkdirAll(outDir, os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
	}

	var eg errgroup.Group
	svc := rds.NewFromConfig(cfg)

	var marker *string

	for {
		out, err := svc.DescribeDBClusters(ctx, &rds.DescribeDBClustersInput{
			Marker: marker,
		})
		if err != nil {
			log.Fatalf("unable to list RDS Clusters, %v", err)
		}

		for _, cluster := range out.DBClusters {
			cluster := cluster

			eg.Go(func() error {
				return writeClusterJSONReport(outDir, cluster)
			})
		}

		marker = out.Marker

		if marker == nil {
			break
		}
	}

	marker = nil

	for {
		out, err := svc.DescribeDBInstances(ctx, &rds.DescribeDBInstancesInput{
			Marker: marker,
		})
		if err != nil {
			log.Printf("unable to list RDS Instances, %v", err)
		}

		for _, instance := range out.DBInstances {
			instance := instance

			eg.Go(func() error {
				return writeInstanceJSONReport(outDir, instance)
			})
		}

		marker = out.Marker

		if marker == nil {
			break
		}
	}

	err = eg.Wait()
	if err != nil {
		log.Fatal(err)
	}
}

func writeClusterJSONReport(outDir string, cluster awsrdstypes.DBCluster) error {
	if cluster.DBClusterArn == nil {
		return fmt.Errorf("DB Cluster did not contain a DBClusterArn")
	}

	clusterArn, err := arn.Parse(*cluster.DBClusterArn)
	if err != nil {
		return err
	}
	filename := fmt.Sprintf("%s-%s-%s-cluster.json", clusterArn.AccountID, clusterArn.Region, *cluster.DBClusterIdentifier)

	var r awsrds.Report

	r.Database.AccountId = clusterArn.AccountID
	r.Database.Region = clusterArn.Region

	r.Database.ARN = clusterArn.String()

	if cluster.DBClusterIdentifier == nil {
		return fmt.Errorf("Cluster with ARN %s did not have a DBClusterIdentifier", clusterArn.String())
	}
	r.Database.Name = *cluster.DBClusterIdentifier

	if cluster.Engine == nil {
		return fmt.Errorf("Cluster with ARN %s did not have an Engine", clusterArn.String())
	}
	r.Database.Engine.Name = *cluster.Engine

	if cluster.EngineVersion == nil {
		return fmt.Errorf("Cluster with ARN %s did not have an EngineVersion", clusterArn.String())
	}
	r.Database.Engine.Version = *cluster.EngineVersion

	r.Database.Tags = make(map[string]string)

	for _, t := range cluster.TagList {
		if t.Key == nil || t.Value == nil {
			return fmt.Errorf("Cluster with ARN %s contains some tags with nil key/values: %v", clusterArn.String(), cluster.TagList)
		}

		r.Database.Tags[*t.Key] = *t.Value
	}

	data, err := json.Marshal(r)
	if err != nil {
		return err
	}

	err = os.WriteFile(path.Join(outDir, filename), data, 0644)
	if err != nil {
		return err
	}

	return nil
}

func writeInstanceJSONReport(outDir string, instance awsrdstypes.DBInstance) error {
	if instance.DBInstanceArn == nil {
		return fmt.Errorf("DB instance did not contain a DBInstanceArn")
	}

	instanceArn, err := arn.Parse(*instance.DBInstanceArn)
	if err != nil {
		return err
	}

	if instance.DBInstanceIdentifier == nil {
		return fmt.Errorf("instance with ARN %s did not contain a DBInstanceIdentifier", instanceArn.String())
	}

	filename := fmt.Sprintf("%s-%s-%s-instance.json", instanceArn.AccountID, instanceArn.Region, *instance.DBInstanceIdentifier)

	var r awsrds.Report

	r.Database.AccountId = instanceArn.AccountID
	r.Database.Region = instanceArn.Region

	r.Database.ARN = instanceArn.String()

	if instance.DBInstanceIdentifier == nil {
		return fmt.Errorf("instance with ARN %s did not have a DBInstanceIdentifier", instanceArn.String())
	}
	r.Database.Name = *instance.DBInstanceIdentifier

	if instance.Engine == nil {
		return fmt.Errorf("instance with ARN %s did not have an Engine", instanceArn.String())
	}
	r.Database.Engine.Name = *instance.Engine

	if instance.EngineVersion == nil {
		return fmt.Errorf("instance with ARN %s did not have an EngineVersion", instanceArn.String())
	}
	r.Database.Engine.Version = *instance.EngineVersion

	r.Database.Tags = make(map[string]string)

	for _, t := range instance.TagList {
		if t.Key == nil || t.Value == nil {
			return fmt.Errorf("instance with ARN %s contains some tags with nil key/values: %v", instanceArn.String(), instance.TagList)
		}

		r.Database.Tags[*t.Key] = *t.Value
	}

	data, err := json.Marshal(r)
	if err != nil {
		return err
	}

	err = os.WriteFile(path.Join(outDir, filename), data, 0644)
	if err != nil {
		return err
	}

	return nil
}
