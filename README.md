# End of Life version checker

A set of command-line Go tools to list whether software is running end-of-life versions.

See package documentation on [pkg.go.dev](https://pkg.go.dev/gitlab.com/tanna.dev/endoflife-checker).

## AWS Lambda End of Life Checker

A command-line Go tool to list all AWS Lambda functions that are in a given region, and annotating them with information around the [runtime deprecation timelines](https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html).

### Installation

Install the command-line tool by running:

```sh
go install gitlab.com/tanna.dev/endoflife-checker/cmd/aws-lambda-endoflife@HEAD
```

### Usage

#### JSON Report

To get a JSON report - for instance to use with [dependency-management-data](https://dmd.tanna.dev)'s own end-of-life checker - you can run:

```sh
aws-lambda-endoflife -json
```

Which will output files to the `out` directory:

```
012345678901-eu-west-1-lambda-name-here.json
...
```

This can be configured by running:

```sh
# to output to ../results
aws-lambda-endoflife -json -out ../results
```

This takes advantage of AWS Configuration so will query your default profile and default region, which can be configured using the common environment variables, such as:

```sh
env AWS_DEFAULT_REGION=mars-east-1 aws-lambda-endoflife -json
```

#### Tab Separated Value (TSV) Report

To get a Tab Separated Value (TSV) formatted output - handy for putting into a spreadsheet - you can run:

```sh
aws-lambda-endoflife -report
```

This takes advantage of AWS Configuration so will query your default profile and default region, which can be configured using the common environment variables, such as:

```sh
env AWS_DEFAULT_REGION=mars-east-1 aws-lambda-endoflife -report
```

#### Look up a specific runtime's data

To look up a single runtime's data, you can run:

```sh
aws-lambda-endoflife -runtime nodejs12.x
```

## AWS RDS End of Life Checker

A command-line Go tool to list all AWS RDS Clusters that are in a given region, and annotating them with information around the engine deprecation timelines.

### Installation

Install the command-line tool by running:

```sh
go install gitlab.com/tanna.dev/endoflife-checker/cmd/aws-rds-endoflife@HEAD
```

### Usage

#### JSON Report

To get a JSON report - for instance to use with [dependency-management-data](https://dmd.tanna.dev)'s own end-of-life checker - you can run:

```sh
aws-rds-endoflife -json
```

Which will output files to the `out` directory:

```
012345678901-eu-west-1-rds-name-here.json
...
```

This can be configured by running:

```sh
# to output to ../results
aws-rds-endoflife -json -out ../results
```

This takes advantage of AWS Configuration so will query your default profile and default region, which can be configured using the common environment variables, such as:

```sh
env AWS_DEFAULT_REGION=mars-east-1 aws-rds-endoflife -json
```

#### Look up a specific engine's data

To look up a specific RDS engine's data, you can run:

```sh
aws-rds-endoflife -engine aurora-postgresql -engineVersion 15.2
```

## AWS ElastiCache End of Life Checker

A command-line Go tool to list all AWS ElastiCache Clusters that are in a given region, and annotating them with information around the engine deprecation timelines.

### Installation

Install the command-line tool by running:

```sh
go install gitlab.com/tanna.dev/endoflife-checker/cmd/aws-elasticache-endoflife@HEAD
```

### Usage

#### JSON Report

To get a JSON report - for instance to use with [dependency-management-data](https://dmd.tanna.dev)'s own end-of-life checker - you can run:

```sh
aws-elasticache-endoflife -json
```

Which will output files to the `out` directory:

```
012345678901-eu-west-1-elasticache-name-here.json
...
```

This can be configured by running:

```sh
# to output to ../results
aws-elasticache-endoflife -json -out ../results
```

This takes advantage of AWS Configuration so will query your default profile and default region, which can be configured using the common environment variables, such as:

```sh
env AWS_DEFAULT_REGION=mars-east-1 aws-elasticache-endoflife -json
```

#### Look up a specific engine's data

To look up a specific ElastiCache engine's data, you can run:

```sh
aws-elasticache-endoflife -engine redis -engineVersion 3
```

## License

This code is licensed under the Apache-2.0.
