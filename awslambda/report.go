package awslambda

type Report struct {
	Metadata struct{} `json:"metadata"`
	Function struct {
		AccountId    string            `json:"account_id"`
		Region       string            `json:"region"`
		ARN          string            `json:"arn"`
		Name         string            `json:"name"`
		Runtime      string            `json:"runtime"`
		LastModified *string           `json:"last_modified"`
		Tags         map[string]string `json:"tags"`
	} `json:"function"`
}
