// Stores information about the runtimes that are deprecated in AWS Lambda, as per https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html
package runtimes

import (
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/lambda/types"
)

type DeprecationInformation struct {
	// Deprecation marks the "Phase 1" deprecations as noted in https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html:
	// > Phase 1 - Lambda no longer applies security patches or other updates to
	// > the runtime. You can no longer **create** functions that use the runtime, but
	// > you can continue to update existing functions. This includes updating the
	// > runtime version, and rolling back to the previous runtime version. Note
	// > that functions that use a deprecated runtime are no longer eligible for
	// > technical support
	Deprecation *time.Time
	// EndOfLife marks the "Phase 2" deprecations as noted in https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html
	// > Phase 2 - you can no longer **create or update** functions that use the
	// > runtime. To update a function, you need to migrate it to a supported
	// > runtime version. After you migrate the function to a supported runtime
	// > version, you cannot rollback the function to the previous runtime. Phase 2
	// > starts at least 30 days after the start of Phase 1
	EndOfLife *time.Time
}

var deprecationData map[types.Runtime]DeprecationInformation = map[types.Runtime]DeprecationInformation{
	types.RuntimeNodejs12x: {
		Deprecation: mustParse("Mar 31, 2023"),
		EndOfLife:   mustParse("Apr 30, 2023"),
	},
	types.RuntimeDotnetcore31: {
		Deprecation: mustParse("Mar 31, 2023"),
		EndOfLife:   mustParse("May 3, 2023"),
	},
	types.RuntimePython36: {
		Deprecation: mustParse("Jul 18, 2022"),
		EndOfLife:   mustParse("Aug 29, 2022"),
	},
	types.RuntimePython27: {
		Deprecation: mustParse("Jul 15, 2021"),
		EndOfLife:   mustParse("May 30, 2022"),
	},
	types.RuntimeDotnetcore21: {
		Deprecation: mustParse("Jan 5, 2022"),
		EndOfLife:   mustParse("Apr 13, 2022"),
	},
	types.RuntimeRuby25: {
		Deprecation: mustParse("Jul 30, 2021"),
		EndOfLife:   mustParse("Mar 31, 2022"),
	},
	types.RuntimeNodejs10x: {
		Deprecation: mustParse("Jul 30, 2021"),
		EndOfLife:   mustParse("Feb 14, 2022"),
	},
	types.RuntimeNodejs810: {
		EndOfLife: mustParse("Mar 6, 2020"),
	},
	types.RuntimeNodejs43: {
		EndOfLife: mustParse("Mar 5, 2020"),
	},
	types.RuntimeNodejs610: {
		EndOfLife: mustParse("Aug 12, 2019"),
	},
	types.RuntimeDotnetcore10: {
		EndOfLife: mustParse("Jul 30, 2019"),
	},
	types.RuntimeDotnetcore20: {
		EndOfLife: mustParse("May 30, 2019"),
	},
	types.RuntimeNodejs43edge: {
		EndOfLife: mustParse("Apr 30, 2019"),
	},
	types.RuntimeNodejs: {
		EndOfLife: mustParse("Oct 31, 2016"),
	},

	types.RuntimeNodejs16x: {
		Deprecation: mustParse("Oct 10, 2023"),
	},
	types.RuntimeNodejs14x: {
		Deprecation: mustParse("Oct 10, 2023"),
	},
	types.RuntimePython37: {
		Deprecation: mustParse("Oct 10, 2023"),
	},
	types.RuntimeJava8: {
		Deprecation: mustParse("Dec 31, 2023"),
	},
	types.RuntimeGo1x: {
		Deprecation: mustParse("Dec 31, 2023"),
	},
	types.RuntimeRuby27: {
		Deprecation: mustParse("Dec 7, 2023"),
	},
	types.RuntimeProvided: {
		Deprecation: mustParse("Dec 31, 2023"),
	},
}

// DeprecationData returns the deprecation data as this package understands.
// This does NOT currently clone the map, please don't be evil!
func DeprecationData() map[types.Runtime]DeprecationInformation {
	return deprecationData
}

func IsDeprecated(runtime types.Runtime) (DeprecationInformation, bool) {
	data, found := deprecationData[runtime]
	return data, found
}

func mustParse(s string) *time.Time {
	t, err := time.Parse("Jan 2, 2006", s)
	if err != nil {
		panic(fmt.Sprintf("Could not parse date %v, %v\n", s, err))
	}

	return &t
}
