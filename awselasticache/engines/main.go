package engines

import (
	"fmt"
	"time"
)

type DeprecationInformation struct {
	EndOfLife time.Time
}

// https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/deprecated-engine-versions.html
var redisDeprecationData map[string]DeprecationInformation = map[string]DeprecationInformation{
	"3": {
		EndOfLife: mustParse("July 31, 2023"),
	},
	"2": {
		EndOfLife: mustParse("January 13, 2023"),
	},
}

var deprecationData map[string]map[string]DeprecationInformation = map[string]map[string]DeprecationInformation{
	"redis": redisDeprecationData,
}

// DeprecationData returns the deprecation data as this package understands.
// This does NOT currently clone the map, please don't be evil!
func DeprecationData() map[string]map[string]DeprecationInformation {
	return deprecationData
}

func IsDeprecated(engine string, engineVersion string) (DeprecationInformation, bool) {
	engineData, found := deprecationData[engine]
	if !found {
		return DeprecationInformation{}, false
	}

	versionData, found := engineData[engineVersion]
	return versionData, found
}

func mustParse(s string) time.Time {
	t, err := time.Parse("January 2, 2006", s)
	if err != nil {
		panic(fmt.Sprintf("Could not parse date %v, %v\n", s, err))
	}

	return t
}
