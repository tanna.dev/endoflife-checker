package awselasticache

type Report struct {
	Metadata struct{} `json:"metadata"`
	Database struct {
		AccountId string `json:"account_id"`
		Region    string `json:"region"`
		ARN       string `json:"arn"`
		Name      string `json:"name"`
		Engine    struct {
			Name    string `json:"name"`
			Version string `json:"version"`
		} `json:"engine"`
		Tags map[string]string `json:"tags"`
	} `json:"database"`
}
