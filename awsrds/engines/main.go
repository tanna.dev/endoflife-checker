package engines

import (
	"fmt"
	"time"
)

type DeprecationInformation struct {
	Deprecation time.Time
}

// https://docs.aws.amazon.com/AmazonRDS/latest/PostgreSQLReleaseNotes/postgresql-release-calendar.html
var postgresqlDeprecationData map[string]DeprecationInformation = map[string]DeprecationInformation{
	"15.2": {
		Deprecation: mustParse("February 2024"),
	},

	"14.7": {
		Deprecation: mustParse("March 2024"),
	},
	"14.6": {
		Deprecation: mustParse("January 2024"),
	},
	"14.5": {
		Deprecation: mustParse("November 2023"),
	},
	"14.4": {
		Deprecation: mustParse("September 2023"),
	},
	"14.3": {
		Deprecation: mustParse("August 2023"),
	},
	"14.2": {
		Deprecation: mustParse("20 March 2023"),
	},
	"14.1": {
		Deprecation: mustParse("20 March 2023"),
	},

	"13.10": {
		Deprecation: mustParse("March 2024"),
	},
	"13.9": {
		Deprecation: mustParse("January 2024"),
	},
	"13.8": {
		Deprecation: mustParse("November 2023"),
	},
	"13.7": {
		Deprecation: mustParse("August 2023"),
	},
	"13.6": {
		Deprecation: mustParse("20 March 2023"),
	},
	"13.5": {
		Deprecation: mustParse("20 March 2023"),
	},
	"13.4": {
		Deprecation: mustParse("20 March 2023"),
	},
	"13.3": {
		Deprecation: mustParse("20 March 2023"),
	},

	"12.14": {
		Deprecation: mustParse("March 2024"),
	},
	"12.13": {
		Deprecation: mustParse("January 2024"),
	},
	"12.12": {
		Deprecation: mustParse("November 2023"),
	},
	"12.11": {
		Deprecation: mustParse("August 2023"),
	},
	"12.10": {
		Deprecation: mustParse("20 March 2023"),
	},
	"12.9": {
		Deprecation: mustParse("20 March 2023"),
	},
	"12.8": {
		Deprecation: mustParse("20 March 2023"),
	},
	"12.7": {
		Deprecation: mustParse("20 March 2023"),
	},

	"11.19": {
		Deprecation: mustParse("November 2023"),
	},
	"11.18": {
		Deprecation: mustParse("November 2023"),
	},
	"11.17": {
		Deprecation: mustParse("November 2023"),
	},
	"11.16": {
		Deprecation: mustParse("August 2023"),
	},
	"11.15": {
		Deprecation: mustParse("20 March 2023"),
	},
	"11.14": {
		Deprecation: mustParse("20 March 2023"),
	},
	"11.13": {
		Deprecation: mustParse("20 March 2023"),
	},
	"11.12": {
		Deprecation: mustParse("20 March 2023"),
	},

	"10.23": {
		Deprecation: mustParse("17 April 2023"),
	},
	"10.22": {
		Deprecation: mustParse("17 April 2023"),
	},
	"10.21": {
		Deprecation: mustParse("17 April 2023"),
	},
	"10.20": {
		Deprecation: mustParse("20 March 2023"),
	},
	"10.19": {
		Deprecation: mustParse("20 March 2023"),
	},
	"10.18": {
		Deprecation: mustParse("20 March 2023"),
	},
	"10.17": {
		Deprecation: mustParse("20 March 2023"),
	},
}

// Via https://docs.aws.amazon.com/AmazonRDS/latest/AuroraPostgreSQLReleaseNotes/AuroraPostgreSQL.Updates.html
var auroraPostgresqlDeprecationData map[string]DeprecationInformation = map[string]DeprecationInformation{
	"13.3": {
		Deprecation: mustParse("March 15, 2023"),
	},
	"12.7": {
		Deprecation: mustParse("March 15, 2023"),
	},
	"12.6": {
		Deprecation: mustParse("July 15, 2022"),
	},
	"12.4": {
		Deprecation: mustParse("July 15, 2022"),
	},
	"11.12": {
		Deprecation: mustParse("March 15, 2023"),
	},
	"11.11": {
		Deprecation: mustParse("July 15, 2022"),
	},
	"11.8": {
		Deprecation: mustParse("July 15, 2022"),
	},
	"11.7": {
		Deprecation: mustParse("March 15, 2022"),
	},
	"11.6": {
		Deprecation: mustParse("March 15, 2022"),
	},
	"11.4": {
		Deprecation: mustParse("October 28, 2021"),
	},
	"10.21": {
		Deprecation: mustParse("August 1, 2022"),
	},
	"10.20": {
		Deprecation: mustParse("August 1, 2022"),
	},
	"10.19": {
		Deprecation: mustParse("August 1, 2022"),
	},
	"10.18": {
		Deprecation: mustParse("August 1, 2022"),
	},
	"10.17": {
		Deprecation: mustParse("August 1, 2022"),
	},
	"10.16": {
		Deprecation: mustParse("July 15, 2022"),
	},
	"10.14": {
		Deprecation: mustParse("July 15, 2022"),
	},
	"10.13": {
		Deprecation: mustParse("July 15, 2022"),
	},
	"10.12": {
		Deprecation: mustParse("March 15, 2022"),
	},
	"10.11": {
		Deprecation: mustParse("March 15, 2022"),
	},
	"10.7": {
		Deprecation: mustParse("October 28, 2021"),
	},
	"10.6": {
		Deprecation: mustParse("October 28, 2021"),
	},
	"10.5": {
		Deprecation: mustParse("October 28, 2021"),
	},
	"10.4": {
		Deprecation: mustParse("October 28, 2021"),
	},
}

var deprecationData map[string]map[string]DeprecationInformation = map[string]map[string]DeprecationInformation{
	"aurora-postgresql": postgresqlDeprecationData,
	"postgres":          postgresqlDeprecationData,
}

// DeprecationData returns the deprecation data as this package understands.
// This does NOT currently clone the map, please don't be evil!
func DeprecationData() map[string]map[string]DeprecationInformation {
	return deprecationData
}

func IsDeprecated(engine string, engineVersion string) (DeprecationInformation, bool) {
	engineData, found := deprecationData[engine]
	if !found {
		return DeprecationInformation{}, false
	}

	versionData, found := engineData[engineVersion]
	return versionData, found
}

func mustParse(s string) time.Time {
	t, err := time.Parse("2 January 2006", s)
	if err == nil {
		return t
	}

	t, err = time.Parse("January 2006", s)
	if err == nil {
		return t
	}

	t, err = time.Parse("January 2, 2006", s)
	if err == nil {
		return t
	}

	panic(fmt.Sprintf("Could not parse date %v, %v\n", s, err))
}
